import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
import sys

def main(args):


    displays=0
    if len(args)== 3 and args[2]=="displays":
        displays = 1

    # Load the cascade
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')


    dir_path = os.path.dirname(os.path.realpath(__file__))
    orgin_dir=dir_path

    #os differentiation
    dir_char='/'
    if os.name=='nt':
        dir_char='\\'

    user_images_directory = 'images_directory'
    user_results_dir='results_dir'
    #args processing
    if  len(args)> 0 :
        user_images_directory = args[0]

    if len(args) >1:
        user_results_dir = args[1]

    if os.name=='nt':
        user_images_directory.replace('/', '\\')
        user_results_dir.replace('/', '\\')

    images_dir=orgin_dir+dir_char+user_images_directory
    images_results_dir=orgin_dir+dir_char+user_results_dir

    if not os.path.isdir(images_dir):
        print ("%s is not a directory! Abort!" % images_dir)
        sys.exit()


    if not os.path.isdir(images_results_dir):
        try:
            os.mkdir(images_results_dir)
        except OSError:
            print ("Creation of the directory %s failed" % images_results_dir)
            sys.exit()
        else:
            print ("Successfully created the directory %s " % images_results_dir)

    file_names = []

    #get files names
    for root, dirs, files in os.walk(images_dir):
        for filename in files:
            #print(filename)
            file_names.append(filename)



    if displays:
        print('Number of images to process {}.'.format(len(file_names)))

    results_files=[]
    for file in file_names:
    # Read the input image

        img = cv2.imread(images_dir+"/"+file)

        # Convert into grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Detect faces
        faces = face_cascade.detectMultiScale(gray, 1.25, 4)
        # Draw rectangle around the faces
        face_number=0
        for (x, y, w, h) in faces:
            img2=img.copy()
            face_number+=1
            cv2.rectangle(img2, (x, y), (x+w, y+h), (255, 0, 0), 2)
            results_files.append([img[y:y+h,x:x+w],file,face_number])
            # Display the output
            if displays:
                print('x : {}, y : {}, w : {}, h : {}'.format(x,y,w,h))
                cv2.imshow('img', img2)
                cv2.waitKey()
    if displays:
        print('Saving {} faces.'.format(len(results_files)))
    for image, file,face_number in results_files:
        #resize image to 300x300 for the
        image = cv2.resize(image,(300,300))
        #save images
        os.chdir(images_results_dir)
        file_name=file.split('.')[0]
        # Filename
        filename = ''+file_name+'_'+str(face_number)+'.png'

        # Using cv2.imwrite() method
        # Saving the image
        cv2.imwrite(filename, image)

    os.chdir(orgin_dir)

main(sys.argv[1:])
