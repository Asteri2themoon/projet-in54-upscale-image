import torch
import torch.nn as nn

import numpy as np

from dataset_celeba import get_loader
from srgan import Discriminator, Generator, train

import matplotlib.pyplot as plt

print('load dataset')
train_loader, test_loader = get_loader(16)

gen = Generator(residual_blocks = 16)
gen.load_state_dict(torch.load('gen.pth', map_location=torch.device('cpu')))
gen.eval()

downsample = nn.AvgPool2d(4)
for x,_ in test_loader:
    break
x_downscale = downsample(x)
x_upscale = gen(x_downscale)

plt.subplot(131)
plt.imshow(np.transpose(x[0].detach()*0.5+0.5,(1,2,0)))
plt.subplot(132)
plt.imshow(np.transpose(x_downscale[0].detach()*0.5+0.5,(1,2,0)))
plt.subplot(133)
plt.imshow(np.transpose(x_upscale[0].detach()*0.5+0.5,(1,2,0)))
plt.show()