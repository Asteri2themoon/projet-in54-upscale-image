import torch
import torch.nn as nn

import numpy as np

from dataset_celeba import get_loader
from srgan import Discriminator, Generator, train

import matplotlib.pyplot as plt

print('load dataset')
train_loader, test_loader = get_loader(16)

# SRGAN
dis = Discriminator(input_size = (128,128))
gen = Generator(residual_blocks = 16)

print('train')
train(train_loader, gen, dis)
