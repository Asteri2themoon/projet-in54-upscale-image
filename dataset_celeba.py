import torch
import torchvision
import pandas as pd
import numpy as np

#simple parser
def parse_attributes(file_name : str = './files/celeba/list_attr_celeba.txt'):
    with open(file_name,'r') as file:
        lines = file.read().replace(' \n','\n').replace('  ',' ').split('\n')

        row_count = int(lines[0])
        header = lines[1].split(' ')
        data = np.empty((row_count,len(header)))

        for i,line in enumerate(lines[2:]):
            attributes = line.split(' ')
            if len(attributes)-1 == len(header):
                data[i,:] = np.array([float(attr) for attr in attributes[1:]]).astype(float)
    
        df = pd.DataFrame(data,columns = header)
        return df
    raise Exception('Can\'t load file {}'.format(file_name))

def get_loader(batch_size : int,size : int = 128, split : float = 0.05, directory : str = './files/celeba'):
    assert batch_size > 0
    assert size > 0
    assert 0 < split < 1

    transform = torchvision.transforms.Compose([
        #torchvision.transforms.Scale(image_size),
        torchvision.transforms.Resize((size,size)),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    dataset = torchvision.datasets.ImageFolder(directory, transform)

    test_length = int(split * len(dataset))
    train_length = len(dataset) - test_length

    training,testing = torch.utils.data.random_split(dataset, [train_length,test_length])

    train_loader = torch.utils.data.DataLoader(dataset=training, batch_size=batch_size, shuffle=True, num_workers=8, drop_last=True)
    test_loader = torch.utils.data.DataLoader(dataset=testing, batch_size=batch_size, shuffle=True, num_workers=8, drop_last=True)

    return train_loader, test_loader
