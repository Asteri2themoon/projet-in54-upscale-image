import torch
import torch.nn as nn
import torch.nn.functional as K
from torch.autograd import Variable

import matplotlib.pyplot as plt

class Discriminator(nn.Module):
    def __init__(self, input_size = (128,128)):
        super(Discriminator,self).__init__()

        (w,h) = input_size
        w,h = w//16,h//16
        
        self.model = nn.Sequential(
            nn.Conv2d(3, 64, 3),
            nn.LeakyReLU(),
            self.block(n_input = 64, n = 64, s = 2),
            self.block(n_input = 64, n = 128, s = 1),
            self.block(n_input = 128, n = 128, s = 2),
            self.block(n_input = 128, n = 256, s = 1),
            self.block(n_input = 256, n = 256, s = 2),
            self.block(n_input = 256, n = 512, s = 1),
            self.block(n_input = 512, n = 512, s = 2),
            nn.Flatten(),
            nn.Linear(w*h*512, 1024),
            nn.LeakyReLU(),
            nn.Linear(1024, 1),
            nn.Sigmoid()
        )
    
    @staticmethod
    def block(n_input : int = 64, n : int = 64, s : int = 1):
        assert n_input > 0
        assert n > 0
        assert s > 0

        block = nn.Sequential(
            nn.Conv2d(n_input, n, 3,
                        padding = 1, stride = s),
            nn.BatchNorm2d(n),
            nn.LeakyReLU()
        )
        return block

    def forward(self,x):
        return self.model(x)

class ResidualBlock(nn.Module):
    def __init__(self, filter_count : int = 64):
        super(ResidualBlock,self).__init__()

        self.convolutions = nn.Sequential(
            nn.Conv2d(filter_count, filter_count, 3, padding = 1),
            nn.BatchNorm2d(filter_count),
            nn.PReLU(),
            nn.Conv2d(filter_count, filter_count, 3, padding = 1),
            nn.BatchNorm2d(filter_count),
        )

    def forward(self,x):
        return self.convolutions(x) + x

class Generator(nn.Module):
    def __init__(self, residual_blocks = 5):
        super(Generator,self).__init__()

        self.pre_residual = nn.Sequential(
            nn.Conv2d(3, 64, 9, padding = 4),
            nn.PReLU()
        )

        res_blocks = [ResidualBlock() for _ in range(residual_blocks)]
        self.residual_blocks = nn.Sequential(*res_blocks)

        self.post_residual = nn.Sequential(
            nn.Conv2d(64, 64, 3, padding = 1),
            nn.BatchNorm2d(64)
        )

        self.upsampling = nn.Sequential(
            nn.Conv2d(64, 256, 3, padding = 1),
            nn.PixelShuffle(2),
            nn.PReLU(),
            nn.Conv2d(64, 256, 3, padding = 1),
            nn.PixelShuffle(2),
            nn.PReLU()
        )

        self.output_layer = nn.Conv2d(64, 3, 9, padding = 4)

    def forward(self, x):
        pre_res = self.pre_residual(x)

        res = self.residual_blocks(pre_res)

        post_res = self.post_residual(res) + pre_res

        upsample = self.upsampling(post_res)

        x_tilde = self.output_layer(upsample)

        return x_tilde

import sys

def train(train_loader, gen, dis, n = 1000, log_interval = 10):
    for x,_ in train_loader:
        batch_size = x.size()[0]
        break

    valid = Variable(torch.ones((batch_size,1)), requires_grad=False)
    fake = Variable(torch.zeros((batch_size,1)), requires_grad=False)
    
    optimizer_dis = torch.optim.Adam(dis.parameters(), lr=1e-4)
    optimizer_gen = torch.optim.Adam(gen.parameters(), lr=1e-4)

    adv_loss = nn.BCELoss()
    mse_loss = nn.MSELoss()

    downsample = nn.AvgPool2d(4)
    
    gen_losses = []
    dis_losses = []

    batch_count = len(train_loader)
    for i in range(n):
        for j,(x,_) in enumerate(train_loader):
            x_downscale = downsample(x)

            gen.eval()
            dis.train()
            optimizer_dis.zero_grad()

            x_tilde = gen(x_downscale)
            l_valid = adv_loss(dis(x),valid)
            l_fake = adv_loss(dis(x_tilde),fake)
            loss_dis = 0.5*(l_valid + l_fake)

            loss_dis.backward()
            optimizer_dis.step()
            
            gen.train()
            dis.eval()
            optimizer_gen.zero_grad()
            
            x_tilde = gen(x_downscale)
            l_mse = mse_loss(x_tilde, x)
            l_gen = adv_loss(dis(x_tilde),valid)

            loss_gen = l_mse + 1e-3*l_gen

            loss_gen.backward()
            optimizer_gen.step()

            gen_losses.append(loss_gen.item())
            dis_losses.append(loss_dis.item())
            print('Loss epoch:{} ({}/{}) dis:{:.4f} gen:{:.4f}'.format(i,j,
                    batch_count,loss_dis.item(),loss_gen.item()))
            
            if (j % log_interval) == 0:
                plt.plot(gen_losses)
                plt.plot(dis_losses)
                plt.title('SRGAN training')
                plt.xlabel('batch')
                plt.ylabel('loss')
                plt.yscale('log')
                plt.legend(['generator','discriminator'])
                plt.savefig('training.svg')
                plt.close()
    
    return gen_losses, dis_losses

